/* ## nwr_mem.h - v1.0.1 - public domain - patreon.riscy.tv
 * *custom allocator building blocks inspired by Andrei Alexandrescu's std.experimental.allocator*
 *
 * ### Preface
 * This library provides building blocks useful for implementing custom allocators.
 * Non-goals are robustness, genericity, and thread-safety. nwr_mem.h strives to
 * make the common case fast. Intentionally unsafe assumptions in the code are
 * clearly documented with comments marked @ASSUMES.
 *                                                                          @TOC
 * ### Table of Contents --------------------- Search For ---------------------
 *  1. Quick Notes                             @README
 *  2. API Quick Reference                     @REF
 *  3. Region Overview                         !Region
 *  4. StackRegion Overview                    !StackRegion
 *  5. Utilities Overview                      !Util
 *  6. Contributors                            @TY
 *  7. Region API                              #Region
 *  8. StackRegion API                         #StackRegion
 *  9. Utilities API                           #Util
 * 10. Region Implementation                   @Region
 * 11. StackRegion Implementation              @StackRegion
 * 12. Utilities Implementation                @Util
 * 13. CHANGELOG                               @LOG
 * 14. LICENSE                                 @LIC
 *                                                                       @README
 * ### Quick Notes
 * Do this:
 *
 *     #define NWR_MEM_IMPLEMENTATION
 *
 * before you include this file in *one* C or C++ file to create the implementation.
 * i.e. it should look like this:
 *
 *     #include ...
 *     #include ...
 *     #include ...
 *     #define NWR_MEM_IMPLEMENTATION
 *     #include "nwr_mem.h"
 *
 * You can `#define NWR_ASSERT(x,s)` before the #include to avoid using assert.h.
 * Define your own `nwr_bool_type`, `nwr_size_type`, and `nwr_u32_type` to avoid
 * using stdbool.h, stddef.h, and stdint.h respectively.
 * Finally, define your own `nwr_memcpy` to avoid using string.h.
 *
 * To override the default alignment, `#define NWR_MEM_DEFAULT_ALIGNMENT n`, where
 * n is the alignment you want. This must be a power of 2. Default alignment is
 * used whenever you pass 0 into an alignment parameter.
 * To disable default alignment, `#define NWR_MEM_NO_DEFAULT_ALIGNMENT`. THIS WILL
 * LIKELY BREAK YOUR CODE if you ever pass 0 into an alignment parameter.
 *
 * If you only want one specific allocator you can `#define NWR_ONLY_X` before
 * the include, where X is the allocator you want. E.g.
 *
 *     #define NWR_ONLY_REGION
 *     #define NWR_MEM_IMPLEMENTATION
 *     #include "nwr_mem.h"
 *
 * This library contains an integer constant `NWR_MEM_VERSION` which will be
 * bumped any time the API undergoes a change that breaks backwards compatibility.
 *                                                                          @REF
 * ### API Quick Reference
 * The actual api is all snake case. By default, all procedures and types are
 * prefixed by `nwr_` and all types are suffixed by `_type`. These can be changed
 * by defining custom `NWR_MEM_DECORATE` and `NWR_MEM_DECORATE_T` macros. E.g.
 *
 * `#define NWR_MEM_DECORATE(s) proc_##s` makes Region.alloc `proc_region_alloc`
 *
 * `#define NWR_MEM_DECORATE_T(s) my_##s##_type` makes Region `my_region_type`
 *
 * Note that nwr_bool_type, nwr_memcpy, nwr_size_type, and nwr_u32_type are macros
 * and thus the decoration rules above do not apply to them.
 *
 * Procedures are prefixed by the allocator they take. For example, if you want
 * to call `Region.isSubRegionAlive`, (with default decoration) you say:
 *
 *     bool result = nwr_region_is_subregion_alive(&region, ptr, len);
 *
 * * Region (nwr_region_type struct, nwr_region_* procedures)
 *   * alignedAlloc      (Region*,len,alignment)  -> ptr to allocation
 *   * alloc             (Region*,len          )  -> ptr to allocation
 *   * allocAll          (Region*,out len*     )  -> ptr to allocation
 *   * available         (Region*              )  -> len available
 *   * create            (ptr    ,len,alignment)  -> Region
 *   * createInSitu      (ptr    ,len,alignment)  -> Region*
 *   * expand            (Region*,ptr,len,delta)  -> delta or 0 on failure
 *   * isAlive           (Region*,ptr          )  -> ptr is alive?
 *   * isEmpty           (Region*              )  -> region is empty?
 *   * isFull            (Region*              )  -> region is full?
 *   * isInside          (Region*,ptr          )  -> ptr is inside?
 *   * isSubRegion       (Region*,ptr,len      )  -> ptr,len is subregion?
 *   * isSubRegionAlive  (Region*,ptr,len      )  -> ptr,len is subregion & alive?
 *   * reset             (Region*              )  -> void
 *
 * * StackRegion (nwr_region_type struct, nwr_stack_region_* procedures)
 *   * alignedAlloc      (Region*,len,alignment)  -> ptr to allocation
 *   * alloc             (Region*,len          )  -> ptr to allocation
 *   * allocAll          (Region*,out len*     )  -> ptr to allocation
 *   * available         (Region*              )  -> len available
 *   * create            (ptr    ,len,alignment)  -> Region
 *   * createInSitu      (ptr    ,len,alignment)  -> Region*
 *   * isAlive           (Region*,ptr          )  -> ptr is alive?
 *   * isEmpty           (Region*              )  -> region is empty?
 *   * isFull            (Region*              )  -> region is full?
 *   * isInside          (Region*,ptr          )  -> ptr is inside?
 *   * isSubRegion       (Region*,ptr,len      )  -> ptr,len is subregion?
 *   * isSubRegionAlive  (Region*,ptr,len      )  -> ptr,len is subregion & alive?
 *   * reset             (Region*              )  -> void
 *
 * * Util (NOTE: these procedures are NOT prefixed with util_)
 *   * isPowerOf2               (n            )  -> n is power of 2?
 *   * roundDownToAlignment     (n  ,alignment)  -> n rounded down to alignment
 *   * roundPtrDownToAlignment  (ptr,alignment)  -> ptr rounded down to alignment
 *   * roundPtrUpToAlignment    (ptr,alignment)  -> ptr rounded up to alignment
 *   * roundUpToAlignment       (n  ,alignment)  -> n rounded up to alignment
 *                                                                       !Region
 * ### Region Overview
 * `Region` is a linear allocator, also known as an Arena. An Arena is the
 * simplest type of allocator -- merely bumping a pointer forward. This type of
 * allocator is useful when you need cheap & fast allocations and are willing to
 * only free in batch.
 *
 * `Region` grows up in memory. If you used `NWR_REGION_GROWS_DOWN` from
 * nwr_mem.h v0.1.0, those features now exist as a seperate api (StackRegion).
 * The `flags` field has been retained as it fills space that would be padded
 * anyway and can be used for your own hacks. Note that `create` no longer takes
 * `flags` as a parameter, and simply zeroes it.
 *
 * `dealloc` from nwr_mem.h v0.1.0 has been removed as it was potentially
 * confusing and rarely needed. Instead, directly set the `current` pointer if
 * you really want to drop memory from the top of the region.
 *                                                                  !StackRegion
 * ### StackRegion Overview
 * `StackRegion` is a linear allocator that grows down (towards lower addresses)
 * in memory, like the stack on many systems (thus the name). This is mainly
 * useful as a building block for double ended allocation schemes. Another use
 * is having a backing store on the system stack (given that the system stack
 * grows downwards) because StackRegion uses hot memory (high addresses) first.
 *
 * `StackRegion` uses the same struct as `Region`, but a seperate set of
 * procedures (don't mix them!) `StackRegion` supports all operations `Region`
 * supports except for `expand.`
 *
 * nwr_mem.h v0.1.0 had the Region and StackRegion integrated with a flag
 * specifying the direction of growth. StackRegion was seperated out from Region
 * so that you don't have to pay for both when you only want one (as is commonly
 * the case).
 *                                                                         !Util
 * ### Utilities Overview
 * A collection of little utility routines deemed useful enough to expose
 * through a public API. These currently include checking whether a number is
 * a power of 2, and rounding numbers and pointers up and down to a specified
 * alignment.
 *
 * nwr_mem.h v0.1.0 had utility routines as macros for internal use only. As of
 * nwr_mem.h v1.0.0, utility routines are now included as a public facing API.
 * While you can exclude the symbols from being exposed publicly, they will still
 * get pulled in for internal use by nwr_mem.h. To support decoration, they are
 * no longer implemented as macros.
 *                                                                           @TY
 * ### Contributors
 * * Allocators
 *     * Neo Ar (Region, StackRegion)
 * * Utilities
 *     * Neo Ar (isPowerOf2, rounding routines)
 * * Inspiration / Special Thanks
 *     * Andrei Alexandrescu (std.experimental.allocator)
 *     * Ginger Bill (Memory Allocation Strategies blog series)
 *     * Henry S. Warren, Jr. (Hacker's Delight book)
 *     * Sean Barrett (stb libs)
 *     * All my patrons (motivation/funding) */
#ifndef NWR_MEM_H_INCLUDED
#define NWR_MEM_H_INCLUDED
#define NWR_MEM_VERSION 1
#ifndef NWR_MEM_DECORATE
#define NWR_MEM_DECORATE(s) nwr_##s
#endif //#ifndef NWR_MEM_DECORATE
#ifndef NWR_MEM_DECORATE_T
#define NWR_MEM_DECORATE_T(s) nwr_##s##_type
#endif //#ifndef NWR_MEM_DECORATE_T
#ifdef __cplusplus
extern "C" {
#endif //#ifdef __cplusplus
#ifndef NWR_ASSERT
#include <assert.h>
#define NWR_ASSERT(x,s) assert((x) && s)
#endif //#ifndef NWR_ASSERT
#ifdef NWR_MEM_STATIC
#define NWR_DEF static
#else
#define NWR_DEF extern
#endif //#ifdef NWR_MEM_STATIC
#if defined(__GNUC__) || defined(__clang__) || defined(__CC_ARM)
#define NWR_INLINE __attribute__((always_inline)) inline
#elif defined(_MSC_VER)
#define NWR_INLINE __forceinline
#else
#define NWR_INLINE inline
#endif //#if defined(__GNUC__) || defined(__clang__) || defined(__CC_ARM)
#ifndef NWR_MEM_DEFAULT_ALIGNMENT
#define NWR_MEM_DEFAULT_ALIGNMENT 16
#endif //#ifndef NWR_MEM_DEFAULT_ALIGNMENT
#ifndef nwr_bool_type
#ifndef __cplusplus
#include <stdbool.h>
#endif //#ifndef __cplusplus
#define nwr_bool_type bool
#endif //#ifndef nwr_bool_type
#ifndef nwr_size_type
#include <stddef.h>
#define nwr_size_type size_t
#endif //#ifndef nwr_size_type
#ifndef nwr_u32_type
#include <stdint.h>
#define nwr_u32_type uint32_t
#endif //#ifndef nwr_u32_type
#ifndef nwr_memcpy
#include <string.h>
#define nwr_memcpy memcpy
#endif //#ifndef nwr_memcpy
#if defined(NWR_ONLY_REGION) \
    || defined(NWR_ONLY_STACK_REGION) \
    || defined(NWR_ONLY_UTIL)
#ifndef NWR_ONLY_REGION
#define NWR_NO_REGION
#endif //#ifndef NWR_ONLY_REGION
#ifndef NWR_ONLY_STACK_REGION
#define NWR_NO_STACK_REGION
#endif //#ifndef NWR_ONLY_STACK_REGION
#ifndef NWR_ONLY_UTIL
#define NWR_NO_UTIL
#endif //#ifndef NWR_ONLY_UTIL
#endif //#if defined(NWR_ONLY_*)
#if !(defined(NWR_NO_REGION) && defined(NWR_NO_STACK_REGION))
typedef struct
{
    /* begin: READ-ONLY
     *     First allocatable byte of Region;
     *     last allocatable byte of StackRegion. */
    void * begin;
    /* current: READ-WRITE
     *     Bumped on allocation.
     *     Pointer to start of free space in Region;
     *     pointer to start of last allocation in StackRegion.
     *
     *     To make a temporary memory savepoint: read `current` into a variable,
     *     do some temporary allocations, and then write the variable back to
     *     `current` to free the temporary storage. */
    void * current;
    /* end: READ-ONLY
     *     Last allocatable byte of Region;
     *     first allocatable byte of StackRegion. */
    void * end;
    /* alignment: READ-ONLY
     *     All Region & StackRegion allocations end on this boundary. */
    nwr_u32_type alignment;
    /* flags: READ-WRITE
     *     nwr_mem.h won't touch this. Padding that you are free to use for your
     *     own hacks. */
    nwr_u32_type flags;
} NWR_MEM_DECORATE_T(region);
#endif //#if !(defined(NWR_NO_REGION) && defined(NWR_NO_STACK_REGION))
#ifndef NWR_NO_REGION
/* #Region.alignedAlloc
 * Allocates from `region` at least `n` bytes aligned to `a`
 * @INPUT: If `a` is 0, aligns to NWR_MEM_DEFAULT_ALIGNMENT
 * @RETURNS: Pointer to allocation or NULL on failure
 * @ASSUMES: Single-threaded usage. Modifies `region` but rolls back on failure.
 *           Does not handle overflow (we aren't near end of address space).
 *           `region` is valid, `n != 0`, `a` is either 0 or a power of 2,
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)`
 * @EXAMPLE:
 *     void * ptr = nwr_region_aligned_alloc(&region, 16, 32);
 *     assert(ptr && ((size_t)ptr & 31) == 0); */
NWR_DEF void *
NWR_MEM_DECORATE(region_aligned_alloc)
    (NWR_MEM_DECORATE_T(region) * region, nwr_size_type n, nwr_u32_type a);
/* #Region.alloc
 * Allocates from `region` at least `n` bytes aligned to `region->alignment`
 * @RETURNS: Pointer to allocation or NULL on failure
 * @ASSUMES: Single-threaded usage. Modifies `region` but rolls back on failure.
 *           Does not handle overflow (we aren't near end of address space).
 *           `region` is valid, `n != 0`
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)`
 * @EXAMPLE:
 *     //assume `string` is u8*, stb stretchy_buffer style, no null-terminator
 *     char * string_to_cstr(nwr_region_type * r, string s)
 *     {
 *         size_t old_len = stb_sb_count(s);
 *         char * cstr = nwr_region_alloc(r, old_len + 1);
 *         memcpy(cstr, s, old_len);
 *         cstr[old_len] = '\0';
 *         return cstr;
 *     } */
NWR_DEF void *
NWR_MEM_DECORATE(region_alloc)
    (NWR_MEM_DECORATE_T(region) * region, nwr_size_type n);
/* #Region.allocAll
 * Allocates all remaining memory from `region` and optionally reports the `size`
 * @OUTPUT: `size` reports the size of the allocation in bytes, ignored if NULL
 * @RETURNS: Pointer to allocation or NULL on failure
 * @ASSUMES: Single-threaded usage. `region` is valid
 * @EXAMPLE:
 *     size_t size;
 *     void * ptr = nwr_region_alloc_all(&region, &size);
 *     assert(ptr && nwr_region_is_full(&region));
 *     printf("%zu\n", size);
 *     //you can pass NULL in for `size` if you don't care
 *     nwr_region_reset(&region);
 *     ptr = nwr_region_alloc_all(&region, NULL);
 *     assert(ptr && nwr_region_is_full(&region)); */
NWR_DEF void *
NWR_MEM_DECORATE(region_alloc_all)
    (NWR_MEM_DECORATE_T(region) * region, nwr_size_type * size);
/* #Region.available
 * Queries the available memory from `region`
 * @NOTE: May be less than the size of the backing store due to alignment
 * @RETURNS: The available memory in bytes
 * @ASSUMES: Single-threaded usage. `region` is valid
 * @EXAMPLE:
 *     size_t mem_len = 1000;
 *     void * aligned_mem = malloc(mem_len+1);
 *     void * misaligned_mem = (void *)((size_t)aligned_mem + 1);
 *     nwr_region_type region = nwr_region_create(misaligned_mem, mem_len, 0);
 *     //the memory `region` uses will be less than the 1000 bytes that
 *     //`misaligned_mem` has available
 *     //let's find out how much space the region can actually use:
 *     size_t avail = nwr_region_available(&region);
 *     printf("%zu\n", avail); */
NWR_INLINE NWR_DEF nwr_size_type
NWR_MEM_DECORATE(region_available)
    (NWR_MEM_DECORATE_T(region) * r);
/* #Region.create
 * Creates a Region to be used with the other Region API calls
 * @NOTE: If you want a region of at least n bytes, the backing store should be
 *        `n + alignment - 1` bytes in size
 * @INPUT: `ptr` is the backing store, `length` is the size of the backing store
 *         in bytes. If `alignment` is 0, aligns to NWR_MEM_DEFAULT_ALIGNMENT.
 *         Otherwise, alignment must be a power of 2
 * @ASSUMES: Single-threaded usage. `ptr` is valid,
 *           Does not handle overflow (we aren't near end of address space).
 *           `alignment` is either 0 or a power of 2,
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)`
 * @EXAMPLE:
 *     //first we get some raw memory from the system
 *     size_t raw_len = 1 << 12;
 *     void * raw_mem = malloc(raw_len);
 *     //next we create a region for fast allocations from our raw mem.
 *     nwr_region_type region = nwr_region_create(raw_mem, raw_len, 0);
 *     //now, let's try a different alignment:
 *     nwr_region_type aregion = nwr_region_create(ptr, len, 64);
 *     //The region may use less memory than what is available in the backing
 *     //store because the region will be aligned within it. You can use
 *     //nwr_region_available to see how much space the region can use. */
NWR_DEF NWR_MEM_DECORATE_T(region)
NWR_MEM_DECORATE(region_create)
    (void * ptr, nwr_size_type length, nwr_u32_type alignment);
/* #Region.createInSitu
 * Same as Region.create except the Region structure lives in the backing store
 * @NOTE: The Region is placed at the beginning of the backing store.
 *        See Region.create for additional documentation
 * @RETURNS: Pointer to the Region structure in the backing store
 * @ASSUMES: Single-threaded usage. `ptr` is valid, `length >= sizeof (Region)`,
 *           Does not handle overflow (we aren't near end of address space).
 *           `alignment` is either 0 or a power of 2,
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)`
 * @EXAMPLE:
 *     nwr_region_type * region_ptr = nwr_region_create_in_situ(raw_mem, raw_len, 0);
 *     assert((void *)region_ptr == raw_mem); */
NWR_DEF NWR_MEM_DECORATE_T(region) *
NWR_MEM_DECORATE(region_create_in_situ)
    (void * ptr, nwr_size_type length, nwr_u32_type alignment);
/* #Region.expand
 * Grows the last allocation from `region` by at least `delta` bytes
 * @NOTE: Only attempts to expand when the ptr,length pair was the last
 *        allocation from `region`. `region->current` won't be bumped in the
 *        case that the delta is small enough to fit within the alignment
 *        padding of the last allocation
 * @INPUT: `ptr` must point to the last allocation from `region`. `length` is
 *         the size (in bytes) requested from the last allocation. `delta` is
 *         the number of bytes you want the allocation expanded by
 * @RETURNS: `delta` on success, 0 on failure
 * @ASSUMES: Single-threaded usage. Modifies `region` but rolls back on failure.
 *           Does not handle overflow (we aren't near end of address space).
 *           `region` is valid, `ptr` is valid, `length` is valid
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)`
 * @EXAMPLE:
 *     nwr_region_create(&region, mem, mem_len, 0);
 *     size_t len = mem_len >> 1;
 *     void * ptr = nwr_region_alloc(&region, len);
 *     size_t delta = nwr_region_expand(&region, ptr, len, len);
 *     assert(ptr && delta == len && nwr_region_is_full(&region));
 *     //expand doesn't need to alloc if the delta fits within padding
 *     nwr_region_reset(&region);
 *     len = mem_len - 16 + 1;
 *     ptr = nwr_region_alloc(&region, len);
 *     void * prev_current = region.current;
 *     delta = nwr_region_expand(&region, ptr, len, 15);
 *     assert(delta == 15 && prev_current == region.current); */
NWR_DEF nwr_size_type
NWR_MEM_DECORATE(region_expand)
    (NWR_MEM_DECORATE_T(region) * region, void * ptr, nwr_size_type length,
     nwr_size_type delta);
/* #Region.isAlive
 * Queries whether `p` is still valid and allocated from `r`
 * @ASSUMES: Single-threaded usage. `r` is valid
 * @EXAMPLE:
 *     void * ptr = nwr_region_alloc(&region, 42);
 *     assert(nwr_region_is_alive(&region, ptr));
 *     nwr_region_reset(&region);
 *     assert(!nwr_region_is_alive(&region, ptr)); */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_alive)
    (NWR_MEM_DECORATE_T(region) * r, void * p);
/* #Region.isEmpty
 * @ASSUMES: Single-threaded usage. `r` is valid
 * @EXAMPLE:
 *     nwr_region_create(&region, ptr, len, 0);
 *     assert(nwr_region_is_empty(&region));
 *     nwr_region_alloc(&region, 42);
 *     assert(!nwr_region_is_empty(&region)); */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_empty)
    (NWR_MEM_DECORATE_T(region) * r);
/* #Region.isFull
 * @ASSUMES: Single-threaded usage. `r` is valid
 * @EXAMPLE:
 *     nwr_region_create(&region, ptr, len, 0);
 *     assert(!nwr_region_is_full(&region));
 *     nwr_region_alloc_all(&region, NULL);
 *     assert(nwr_region_is_full(&region)); */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_full)
    (NWR_MEM_DECORATE_T(region) * r);
/* #Region.isInside
 * Queries whether `p` is in the address space of `r`
 * @ASSUMES: Single-threaded usage. `r` is valid
 * @EXAMPLE:
 *     void * ptr = nwr_region_alloc(&region, 42);
 *     assert(nwr_region_is_inside(&region, ptr));
 *     nwr_region_reset(&region);
 *     assert(nwr_region_is_inside(&region, ptr)); */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_inside)
    (NWR_MEM_DECORATE_T(region) * r, void * p);
/* #Region.isSubRegion
 * Queries whether `l` bytes starting from `p` are fully contained in the
 * address space of `r`
 * @ASSUMES: Single-threaded usage. `r` is valid
 * @EXAMPLE:
 *     size_t len = 42;
 *     void * ptr = nwr_region_alloc(&region, len);
 *     assert(nwr_region_is_subregion(&region, ptr, len));
 *     nwr_region_reset(&region);
 *     assert(nwr_region_is_subregion(&region, ptr, len)); */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_subregion)
    (NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l);
/* #Region.isSubRegionAlive
 * Queries whether `l` bytes starting from `p` are still valid and allocated
 * from `r`
 * @ASSUMES: Single-threaded usage. `r` is valid
 * @EXAMPLE:
 *     size_t len = 42;
 *     void * ptr = nwr_region_alloc(&region, len);
 *     assert(nwr_region_is_subregion_alive(&region, ptr, len));
 *     nwr_region_reset(&region);
 *     assert(!nwr_region_is_subregion_alive(&region, ptr, len)); */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_subregion_alive)
    (NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l);
/* #Region.reset
 * Deallocates everything from `r`
 * @ASSUMES: Single-threaded usage. `r` is valid
 * @EXAMPLE:
 *     if (nwr_region_is_full(&region))
 *         nwr_region_reset(&region);
 *     assert(nwr_region_is_empty(&region)); */
NWR_INLINE NWR_DEF void
NWR_MEM_DECORATE(region_reset)
    (NWR_MEM_DECORATE_T(region) * r);
#endif //NWR_NO_REGION
#ifndef NWR_NO_STACK_REGION
/* #StackRegion.alignedAlloc
 * @NOTE: See !Region, !StackRegion, Region.alignedAlloc */
NWR_DEF void *
NWR_MEM_DECORATE(stack_region_aligned_alloc)
    (NWR_MEM_DECORATE_T(region) * region, nwr_size_type n, nwr_u32_type a);
/* #StackRegion.alloc
 * @NOTE: See !Region, !StackRegion, Region.alloc */
NWR_DEF void *
NWR_MEM_DECORATE(stack_region_alloc)
    (NWR_MEM_DECORATE_T(region) * region, nwr_size_type n);
/* #StackRegion.allocAll
 * @NOTE: See !Region, !StackRegion, Region.allocAll */
NWR_DEF void *
NWR_MEM_DECORATE(stack_region_alloc_all)
    (NWR_MEM_DECORATE_T(region) * region, nwr_size_type * size);
/* #StackRegion.available
 * @NOTE: See !Region, !StackRegion, Region.available */
NWR_INLINE NWR_DEF nwr_size_type
NWR_MEM_DECORATE(stack_region_available)
    (NWR_MEM_DECORATE_T(region) * r);
/* #StackRegion.create
 * @NOTE: See !Region, !StackRegion, Region.create */
NWR_DEF NWR_MEM_DECORATE_T(region)
NWR_MEM_DECORATE(stack_region_create)
    (void * ptr, nwr_size_type length, nwr_u32_type alignment);
/* #StackRegion.createInSitu
 * @NOTE: The Region is placed at the end of the backing store.
 *        See !Region, !StackRegion, Region.createInSitu */
NWR_DEF NWR_MEM_DECORATE_T(region) *
NWR_MEM_DECORATE(stack_region_create_in_situ)
    (void * ptr, nwr_size_type length, nwr_u32_type alignment);
/* #StackRegion.isAlive
 * @NOTE: See !Region, !StackRegion, Region.isAlive */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_alive)
    (NWR_MEM_DECORATE_T(region) * r, void * p);
/* #StackRegion.isEmpty
 * @NOTE: See !Region, !StackRegion, Region.isEmpty */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_empty)
    (NWR_MEM_DECORATE_T(region) * r);
/* #StackRegion.isFull
 * @NOTE: See !Region, !StackRegion, Region.isFull */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_full)
    (NWR_MEM_DECORATE_T(region) * r);
/* #StackRegion.isInside
 * @NOTE: See !Region, !StackRegion, Region.isInside */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_inside)
    (NWR_MEM_DECORATE_T(region) * r, void * p);
/* #StackRegion.isSubRegion
 * @NOTE: See !Region, !StackRegion, Region.isSubRegion */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_subregion)
    (NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l);
/* #StackRegion.isSubRegionAlive
 * @NOTE: See !Region, !StackRegion, Region.isSubRegionAlive */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_subregion_alive)
    (NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l);
/* #StackRegion.reset
 * @NOTE: See !Region, !StackRegion, Region.reset */
NWR_INLINE NWR_DEF void
NWR_MEM_DECORATE(stack_region_reset)
    (NWR_MEM_DECORATE_T(region) * r);
#endif //NWR_NO_STACK_REGION
#ifndef NWR_NO_UTIL
/* #Util.isPowerOf2
 * Queries whether `n` is a power of 2
 * @NOTE: (n & (n - 1)) == 0
 * @ASSUMES: `n != 0`
 * @EXAMPLE:
 *     assert(!nwr_is_power_of_2(42));
 *     assert(nwr_is_power_of_2(64)); */
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(is_power_of_2)
    (nwr_u32_type n);
/* #Util.roundDownToAlignment
 * Rounds `n` down to the nearest multiple of `alignment`
 * @NOTE: n & -alignment
 * @ASSUMES: `alignment` is a power of 2,
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)`
 * @EXAMPLE:
 *     assert(nwr_round_down_to_alignment(42, 16) == 32);
 *     assert(nwr_round_down_to_alignment(48, 16) == 48); */
NWR_INLINE NWR_DEF nwr_size_type
NWR_MEM_DECORATE(round_down_to_alignment)
    (nwr_size_type n, nwr_u32_type alignment);
/* #Util.roundPtrDownToAlignment
 * Same as Util.roundDownToAlignment except `n` is a pointer
 * @NOTE: See Util.roundDownToAlignment for additional documentation
 * @ASSUMES: `alignment` is a power of 2,
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)` */
NWR_INLINE NWR_DEF void *
NWR_MEM_DECORATE(round_ptr_down_to_alignment)
    (void * n, nwr_u32_type alignment);
/* #Util.roundPtrUpToAlignment
 * Same as Util.roundUpToAlignment except `n` is a pointer
 * @NOTE: See Util.roundUpToAlignment for additional documentation
 * @ASSUMES: `alignment` is a power of 2,
 *           Does not handle overflow (we aren't near end of address space).
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)` */
NWR_INLINE NWR_DEF void *
NWR_MEM_DECORATE(round_ptr_up_to_alignment)
    (void * n, nwr_u32_type alignment);
/* #Util.roundUpToAlignment
 * Rounds `n` up to the nearest multiple of `alignment`
 * @NOTE:  (n + alignment - 1) & -alignment
 * @ASSUMES: `alignment` is a power of 2,
 *           Does not handle overflow (we aren't near end of address space).
 *           `sizeof (nwr_size_type) >= sizeof (nwr_u32_type)`
 * @EXAMPLE:
 *     assert(nwr_round_up_to_alignment(42, 16) == 48);
 *     assert(nwr_round_up_to_alignment(48, 16) == 48); */
NWR_INLINE NWR_DEF nwr_size_type
NWR_MEM_DECORATE(round_up_to_alignment)
    (nwr_size_type n, nwr_u32_type alignment);
#endif //#ifndef NWR_NO_UTIL
#ifdef __cplusplus
}
#endif //#ifdef __cplusplus
#endif //#ifndef NWR_MEM_H_INCLUDED
#ifdef NWR_MEM_IMPLEMENTATION
#ifndef NWR_NO_UTIL
#define NWR_UTIL_DEF NWR_DEF
#else
#define NWR_UTIL_DEF static
#endif //#ifndef NWR_NO_UTIL
//@Util.isPowerOf2
NWR_INLINE NWR_UTIL_DEF nwr_bool_type
NWR_MEM_DECORATE(is_power_of_2)
(nwr_u32_type n)
{
    NWR_ASSERT(n, "`n` must not be 0");
    nwr_bool_type result = (n & (n - 1)) == 0;
    return result;
}
//@Util.roundDownToAlignment
NWR_INLINE NWR_UTIL_DEF nwr_size_type
NWR_MEM_DECORATE(round_down_to_alignment)
(nwr_size_type n, nwr_u32_type alignment)
{
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    nwr_size_type result = n & -(nwr_size_type)alignment;
    return result;
}
//@Util.roundPtrDownToAlignment
NWR_INLINE NWR_UTIL_DEF void *
NWR_MEM_DECORATE(round_ptr_down_to_alignment)
(void * n, nwr_u32_type alignment)
{
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    void * result = (void *)((nwr_size_type)n & -(nwr_size_type)alignment);
    return result;
}
//@Util.roundPtrUpToAlignment
NWR_INLINE NWR_UTIL_DEF void *
NWR_MEM_DECORATE(round_ptr_up_to_alignment)
(void * n, nwr_u32_type alignment)
{
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    NWR_ASSERT(~(nwr_size_type)n >= alignment - 1u,
            "Rounding `n` up to nearest multiple of `alignment` will overflow");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    void * result = (void *)(((nwr_size_type)n + alignment - 1u)
            & -(nwr_size_type)alignment);
    return result;
}
//@Util.roundUpToAlignment
NWR_INLINE NWR_UTIL_DEF nwr_size_type
NWR_MEM_DECORATE(round_up_to_alignment)
(nwr_size_type n, nwr_u32_type alignment)
{
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    NWR_ASSERT(~n >= alignment - 1u,
            "Rounding `n` up to nearest multiple of `alignment` will overflow");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    nwr_size_type result = (n + alignment - 1u) & -(nwr_size_type)alignment;
    return result;
}
#undef NWR_UTIL_DEF
#ifndef NWR_NO_REGION
//@Region.alignedAlloc
NWR_DEF void *
NWR_MEM_DECORATE(region_aligned_alloc)
(NWR_MEM_DECORATE_T(region) * region, nwr_size_type n, nwr_u32_type a)
{
    NWR_ASSERT(region, "Invalid region");
    NWR_ASSERT(region->end >= region->current, "Corrupt region");
    NWR_ASSERT(n, "0-length allocations are not supported");
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(region->alignment),
            "`region->alignment` must be a power of 2");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    NWR_ASSERT(~n >= region->alignment - 1u, "Aligning `n` will overflow");
#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    if (!a)
        a = NWR_MEM_DEFAULT_ALIGNMENT;
#endif //#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(a), "`a` must be a power of 2");
    n = NWR_MEM_DECORATE(round_up_to_alignment)(n, region->alignment);
    void * saved = region->current;
    NWR_ASSERT(~(nwr_size_type)region->current >= a - 1u,
            "Aligning `region->current` will overflow");
    region->current = NWR_MEM_DECORATE(round_ptr_up_to_alignment)(
            region->current, a);
    NWR_ASSERT(~(nwr_size_type)region->current >= n, "Allocation will overflow");
    void * result = region->current;
    region->current = (void *)((nwr_size_type)region->current + n);
    if (region->current > region->end) goto OOM;
    return result;
OOM:
    region->current = saved;
    return NULL;
}
//@Region.alloc
NWR_DEF void *
NWR_MEM_DECORATE(region_alloc)
(NWR_MEM_DECORATE_T(region) * region, nwr_size_type n)
{
    NWR_ASSERT(region, "Invalid region");
    NWR_ASSERT(region->end >= region->current, "Corrupt region");
    NWR_ASSERT(n, "0-length allocations are not supported");
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(region->alignment),
            "`region->alignment` must be a power of 2");
    NWR_ASSERT(~n >= region->alignment - 1u, "Aligning `n` will overflow");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    n = NWR_MEM_DECORATE(round_up_to_alignment)(n, region->alignment);
    NWR_ASSERT(~(nwr_size_type)region->current >= n, "Allocation will overflow");
    void * result = region->current;
    region->current = (void *)((nwr_size_type)region->current + n);
    if (region->current > region->end) goto OOM;
    return result;
OOM:
    region->current = result;
    return NULL;
}
//@Region.allocAll
NWR_DEF void *
NWR_MEM_DECORATE(region_alloc_all)
(NWR_MEM_DECORATE_T(region) * region, nwr_size_type * size)
{
    void * result;
    NWR_ASSERT(region, "Invalid region");
    NWR_ASSERT(region->end >= region->current, "Corrupt region");
    nwr_size_type available = NWR_MEM_DECORATE(region_available)(region);
    if (!available) goto REGION_FULL;
    if (size)
        *size = available;
    result = region->current;
    region->current = region->end;
    return result;
REGION_FULL:
    if (size)
        *size = 0;
    return NULL;
}
//@Region.available
NWR_INLINE NWR_DEF nwr_size_type
NWR_MEM_DECORATE(region_available)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->end >= r->current, "Corrupt region");
    nwr_size_type result = (nwr_size_type)r->end - (nwr_size_type)r->current;
    return result;
}
//@Region.create
NWR_DEF NWR_MEM_DECORATE_T(region)
NWR_MEM_DECORATE(region_create)
(void * ptr, nwr_size_type length, nwr_u32_type alignment)
{
    NWR_ASSERT(ptr, "Invalid backing store");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    if (!alignment)
        alignment = NWR_MEM_DEFAULT_ALIGNMENT;
#endif //#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    NWR_ASSERT(~(nwr_size_type)ptr >= alignment - 1u, "Aligning `ptr` will overflow");
    void * b = NWR_MEM_DECORATE(round_ptr_up_to_alignment)(ptr, alignment);
    NWR_ASSERT(~(nwr_size_type)ptr >= length, "Region too large/high in memory");
    void * e = NWR_MEM_DECORATE(round_ptr_down_to_alignment)(
            (void *)((nwr_size_type)ptr + length), alignment);
    NWR_MEM_DECORATE_T(region) result =
    {
        .begin = b,
        .current = b,
        .end = e,
        .alignment = alignment
    };
    return result;
}
//@Region.createInSitu
NWR_DEF NWR_MEM_DECORATE_T(region) *
NWR_MEM_DECORATE(region_create_in_situ)
(void * ptr, nwr_size_type length, nwr_u32_type alignment)
{
    NWR_ASSERT(ptr, "Invalid backing store");
    NWR_ASSERT(length >= sizeof (NWR_MEM_DECORATE_T(region)),
            "Backing store too small");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    NWR_ASSERT(~(nwr_size_type)ptr >= length,
            "In situ Region too large/high in memory");
#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    if (!alignment)
        alignment = NWR_MEM_DEFAULT_ALIGNMENT;
#endif //#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    void * region = ptr;
    ptr = (void *)((nwr_size_type)ptr + sizeof (NWR_MEM_DECORATE_T(region)));
    length -= sizeof (NWR_MEM_DECORATE_T(region));
    NWR_ASSERT(~(nwr_size_type)ptr >= alignment - 1u, "Aligning `ptr` will overflow");
    void * b = NWR_MEM_DECORATE(round_ptr_up_to_alignment)(ptr, alignment);
    void * e = NWR_MEM_DECORATE(round_ptr_down_to_alignment)(
            (void *)((nwr_size_type)ptr + length), alignment);
    NWR_MEM_DECORATE_T(region) result =
    {
        .begin = b,
        .current = b,
        .end = e,
        .alignment = alignment
    };
    nwr_memcpy(region, &result, sizeof (NWR_MEM_DECORATE_T(region)));
    return (NWR_MEM_DECORATE_T(region) *)region;
}
//@Region.expand
NWR_DEF nwr_size_type
NWR_MEM_DECORATE(region_expand)
(NWR_MEM_DECORATE_T(region) * region, void * ptr, nwr_size_type length, nwr_size_type delta)
{
    nwr_size_type rounded_new_length;
    NWR_ASSERT(region, "Invalid region");
    NWR_ASSERT(NWR_MEM_DECORATE(region_is_subregion_alive)(region, ptr, length),
            "Invalid ptr,length pair");
    NWR_ASSERT(~length >= region->alignment - 1u, "Aligning `length` will overflow");
    nwr_size_type rounded_length = NWR_MEM_DECORATE(round_up_to_alignment)(length,
            region->alignment);
    if ((nwr_size_type)ptr + rounded_length != (nwr_size_type)region->current)
        goto CANT_EXPAND;
    NWR_ASSERT(~length >= delta, "Expanding by `delta` will overflow");
    length += delta;
    NWR_ASSERT(~length >= region->alignment - 1u,
            "Aligning `length + delta` will overflow");
    rounded_new_length = NWR_MEM_DECORATE(round_up_to_alignment)(
            length, region->alignment);
    if (rounded_length == rounded_new_length
            || NWR_MEM_DECORATE(region_alloc)(region, delta))
        return delta;
CANT_EXPAND:
    return 0;
}
//@Region.isAlive
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_alive)
(NWR_MEM_DECORATE_T(region) * r, void * p)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    NWR_ASSERT(r->begin <= r->current, "Corrupt region");
    nwr_bool_type result = p >= r->begin && p < r->current;
    return result;
}
//@Region.isEmpty
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_empty)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->begin <= r->current, "Corrupt region");
    nwr_bool_type result = r->current == r->begin;
    return result;
}
//@Region.isFull
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_full)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->end >= r->current, "Corrupt region");
    nwr_bool_type result = r->current == r->end;
    return result;
}
//@Region.isInside
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_inside)
(NWR_MEM_DECORATE_T(region) * r, void * p)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    NWR_ASSERT(r->begin <= r->current, "Corrupt region");
    NWR_ASSERT(r->end >= r->current, "Corrupt region");
    nwr_bool_type result = p >= r->begin && p <= r->end;
    return result;
}
//@Region.isSubRegion
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_subregion)
(NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    NWR_ASSERT(~(nwr_size_type)p >= l, "Subregion overflows address space");
    nwr_bool_type result = p >= r->begin && (void *)((nwr_size_type)p + l) <= r->end;
    return result;
}
//@Region.isSubRegionAlive
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(region_is_subregion_alive)
(NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    NWR_ASSERT(~(nwr_size_type)p >= l, "Subregion overflows address space");
    nwr_bool_type result = p >= r->begin && p < r->current
        && (void *)((nwr_size_type)p + l) <= r->current;
    return result;
}
//@Region.reset
NWR_INLINE NWR_DEF void
NWR_MEM_DECORATE(region_reset)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->begin <= r->current, "Corrupt region");
    r->current = r->begin;
}
#endif //#ifndef NWR_NO_REGION
#ifndef NWR_NO_STACK_REGION
//@StackRegion.alignedAlloc
NWR_DEF void *
NWR_MEM_DECORATE(stack_region_aligned_alloc)
(NWR_MEM_DECORATE_T(region) * region, nwr_size_type n, nwr_u32_type a)
{
    NWR_ASSERT(region, "Invalid region");
    NWR_ASSERT(region->begin <= region->current, "Corrupt region");
    NWR_ASSERT(n, "0-length allocations are not supported");
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(region->alignment),
            "`region->alignment` must be a power of 2");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    NWR_ASSERT(~n >= region->alignment - 1u, "Aligning `n` will overflow");
#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    if (!a)
        a = NWR_MEM_DEFAULT_ALIGNMENT;
#endif //#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(a), "`a` must be a power of 2");
    n = NWR_MEM_DECORATE(round_up_to_alignment)(n, region->alignment);
    void * saved = region->current;
    NWR_ASSERT((nwr_size_type)region->current >= n, "Allocation will underflow");
    region->current = (void *)((nwr_size_type)region->current - n);
    NWR_ASSERT((nwr_size_type)region->current >= a,
            "Aligning `region->current` will underflow");
    region->current = NWR_MEM_DECORATE(round_ptr_down_to_alignment)(
            region->current, a);
    if (region->current < region->begin) goto OOM;
    return region->current;
OOM:
    region->current = saved;
    return NULL;
}
//@StackRegion.alloc
NWR_DEF void *
NWR_MEM_DECORATE(stack_region_alloc)
(NWR_MEM_DECORATE_T(region) * region, nwr_size_type n)
{
    NWR_ASSERT(region, "Invalid region");
    NWR_ASSERT(region->begin <= region->current, "Corrupt region");
    NWR_ASSERT(n, "0-length allocations are not supported");
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(region->alignment),
            "`region->alignment` must be a power of 2");
    NWR_ASSERT(~n >= region->alignment - 1u, "Aligning `n` will overflow");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    n = NWR_MEM_DECORATE(round_up_to_alignment)(n, region->alignment);
    NWR_ASSERT((nwr_size_type)region->current >= n, "Allocation will underflow");
    region->current = (void *)((nwr_size_type)region->current - n);
    if (region->current < region->begin) goto OOM;
    return region->current;
OOM:
    region->current = (void *)((nwr_size_type)region->current + n);
    return NULL;
}
//@StackRegion.allocAll
NWR_DEF void *
NWR_MEM_DECORATE(stack_region_alloc_all)
(NWR_MEM_DECORATE_T(region) * region, nwr_size_type * size)
{
    void * result;
    NWR_ASSERT(region, "Invalid region");
    NWR_ASSERT(region->begin <= region->current, "Corrupt region");
    nwr_size_type available = NWR_MEM_DECORATE(stack_region_available)(region);
    if (!available) goto REGION_FULL;
    if (size)
        *size = available;
    region->current = region->begin;
    result = region->current;
    return result;
REGION_FULL:
    if (size)
        *size = 0;
    return NULL;
}
//@StackRegion.available
NWR_INLINE NWR_DEF nwr_size_type
NWR_MEM_DECORATE(stack_region_available)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->begin <= r->current, "Corrupt region");
    nwr_size_type result = (nwr_size_type)r->current - (nwr_size_type)r->begin;
    return result;
}
//@StackRegion.create
NWR_DEF NWR_MEM_DECORATE_T(region)
NWR_MEM_DECORATE(stack_region_create)
(void * ptr, nwr_size_type length, nwr_u32_type alignment)
{
    NWR_ASSERT(ptr, "Invalid backing store");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    if (!alignment)
        alignment = NWR_MEM_DEFAULT_ALIGNMENT;
#endif //#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    NWR_ASSERT(~(nwr_size_type)ptr >= alignment - 1u, "Aligning `ptr` will overflow");
    void * b = NWR_MEM_DECORATE(round_ptr_up_to_alignment)(ptr, alignment);
    NWR_ASSERT(~(nwr_size_type)ptr >= length, "Region too large/high in memory");
    void * e = NWR_MEM_DECORATE(round_ptr_down_to_alignment)(
            (void *)((nwr_size_type)ptr + length), alignment);
    NWR_MEM_DECORATE_T(region) result =
    {
        .begin = b,
        .current = e,
        .end = e,
        .alignment = alignment
    };
    return result;
}
//@StackRegion.createInSitu
NWR_DEF NWR_MEM_DECORATE_T(region) *
NWR_MEM_DECORATE(stack_region_create_in_situ)
(void * ptr, nwr_size_type length, nwr_u32_type alignment)
{
    NWR_ASSERT(ptr, "Invalid backing store");
    NWR_ASSERT(length >= sizeof (NWR_MEM_DECORATE_T(region)),
            "Backing store too small");
    NWR_ASSERT(sizeof (nwr_size_type) >= sizeof (nwr_u32_type),
            "Platforms < 32-bit are not supported");
    NWR_ASSERT(~(nwr_size_type)ptr >= length,
            "In situ Region too large/high in memory");
#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    if (!alignment)
        alignment = NWR_MEM_DEFAULT_ALIGNMENT;
#endif //#ifndef NWR_MEM_NO_DEFAULT_ALIGNMENT
    NWR_ASSERT(NWR_MEM_DECORATE(is_power_of_2)(alignment),
            "`alignment` must be a power of 2");
    NWR_ASSERT(~(nwr_size_type)ptr >= alignment - 1u, "Aligning `ptr` will overflow");
    void * b = NWR_MEM_DECORATE(round_ptr_up_to_alignment)(ptr, alignment);
    nwr_size_type delta = (nwr_size_type)b - (nwr_size_type)ptr;
    length -= delta;
    void * region = (void *)((nwr_size_type)ptr + length
            - sizeof (NWR_MEM_DECORATE_T(region)));
    length -= sizeof (NWR_MEM_DECORATE_T(region));
    void * e = NWR_MEM_DECORATE(round_ptr_down_to_alignment)(
            (void *)((nwr_size_type)ptr + length), alignment);
    NWR_MEM_DECORATE_T(region) result =
    {
        .begin = b,
        .current = e,
        .end = e,
        .alignment = alignment
    };
    nwr_memcpy(region, &result, sizeof (NWR_MEM_DECORATE_T(region)));
    return (NWR_MEM_DECORATE_T(region) *)region;
}
//@StackRegion.isAlive
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_alive)
(NWR_MEM_DECORATE_T(region) * r, void * p)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    nwr_bool_type result = p <= r->end && p >= r->current;
    return result;
}
//@StackRegion.isEmpty
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_empty)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->end >= r->current, "Corrupt region");
    nwr_bool_type result = r->current == r->end;
    return result;
}
//@StackRegion.isFull
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_full)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->begin <= r->current, "Corrupt region");
    nwr_bool_type result = r->current == r->begin;
    return result;
}
//@StackRegion.isInside
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_inside)
(NWR_MEM_DECORATE_T(region) * r, void * p)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    NWR_ASSERT(r->end >= r->current, "Corrupt region");
    NWR_ASSERT(r->begin <= r->current, "Corrupt region");
    nwr_bool_type result = p <= r->end && p >= r->begin;
    return result;
}
//@StackRegion.isSubRegion
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_subregion)
(NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    NWR_ASSERT(~(nwr_size_type)p >= l, "Subregion overflows address space");
    nwr_bool_type result = (void *)((nwr_size_type)p + l) <= r->end && p >= r->begin;
    return result;
}
//@StackRegion.isSubRegionAlive
NWR_INLINE NWR_DEF nwr_bool_type
NWR_MEM_DECORATE(stack_region_is_subregion_alive)
(NWR_MEM_DECORATE_T(region) * r, void * p, nwr_size_type l)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(p, "Invalid ptr");
    NWR_ASSERT(~(nwr_size_type)p >= l, "Subregion overflows address space");
    nwr_bool_type result = p <= r->end && p >= r->current
        && (void *)((nwr_size_type)p + l) > r->current
        && (void *)((nwr_size_type)p + l) <= r->end;
    return result;
}
//@StackRegion.reset
NWR_INLINE NWR_DEF void
NWR_MEM_DECORATE(stack_region_reset)
(NWR_MEM_DECORATE_T(region) * r)
{
    NWR_ASSERT(r, "Invalid region");
    NWR_ASSERT(r->end >= r->current, "Corrupt region");
    r->current = r->end;
}
#endif //#ifndef NWR_NO_STACK_REGION
#endif //#ifdef NWR_MEM_IMPLEMENTATION
/*                                                                          @LOG
 * ### CHANGELOG
 *     v1.0.1 - Small tweaks to play nice with C++ / other compilers
 *     v1.0.0 - NWR_MEM_DECORATE, NWR_MEM_DECORATE_T, StackRegion, Util API
 *              nwr_bool_type, nwr_memcpy, nwr_size_type, nwr_u32_type
 *              removed all use of `const`, removed Region.dealloc
 *     v0.1.0 - Region Allocator
 *                                                                          @LIC
 * ### LICENSE
 * This software is available under 2 licenses -- choose whichever you prefer.
 *
 * ALTERNATIVE A - MIT License
 * Copyright (c) 2017-2022 Neo Ar
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ALTERNATIVE B - Public Domain (www.unlicense.org)
 * This is free and unencumbered software released into the public domain.
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
 * software, either in source code form or as a compiled binary, for any purpose,
 * commercial or non-commercial, and by any means.
 * In jurisdictions that recognize copyright laws, the author or authors of this
 * software dedicate any and all copyright interest in the software to the public
 * domain. We make this dedication for the benefit of the public at large and to
 * the detriment of our heirs and successors. We intend this dedication to be an
 * overt act of relinquishment in perpetuity of all present and future rights to
 * this software under copyright law.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */
